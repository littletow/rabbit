use axum::{routing::get, Router};

use crate::SharedState;

// 识别码API模块
mod identcode;

/// 错误码
/// 10000 成功
/// 40001 验签失败
/// 40002 参数错误
/// 40003 缓存操作失败
/// 40004 数据库操作失败
/// 40005 账号已经存在

pub fn router(shared_state: SharedState) -> Router {
    Router::new()
        .route("/getidenttoken", get(identcode::get_identcode_token))
        .route("/getidentcontent", get(identcode::get_identcode_content))
        .route("/applyappaccount", get(identcode::apply_app_account))
        .route("/getstatuserfollow", get(identcode::get_stat_userfollow))
        .route("/getsentence", get(identcode::get_sentence))
        .with_state(shared_state)
}
