// 公众号API模块
use crate::SharedState;
use axum::routing::get;
use axum::Router;
pub mod gzh;

/// 公众号路由
pub fn router(shared_state: SharedState) -> Router {
    Router::new()
        .route("/mp", get(gzh::get_mp).post(gzh::post_mp))
        .with_state(shared_state)
}
