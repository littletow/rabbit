use axum::{extract::State, Json};
use serde_json::{json, Value};

use crate::SharedState;

// code 1 成功 -1 错误

// 获取公众号配置
pub async fn get_gzh_config(State(_state): State<SharedState>) -> Json<Value> {
    Json(json!({"code":1,"msg":"成功","data":{"sentence":""}}))
}
