use crate::SharedState;
use axum::{routing::get, Router};

mod web;

pub fn router(shared_state: SharedState) -> Router {
    Router::new()
        .route("/getgzhconfig", get(web::get_gzh_config))
        .with_state(shared_state)
}
