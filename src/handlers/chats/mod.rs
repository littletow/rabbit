use axum::{
    routing::{get, post},
    Router,
};

use crate::SharedState;

// 修改昵称
// 聊天室模块
// 创建房间
// 房间变更名称
// 统计进入房间的人数
mod book;
mod user;
pub fn router(shared_state: SharedState) -> Router {
    Router::new()
        .route("/getvcode1109", get(user::get_vcode_1109))
        .route("/login1109", post(user::login_1109))
        .route("/ws1111", get(user::ws_connect_1111))
        .route("/uptname1111", post(user::update_nickname_1111))
        .route("/leavemessage", post(book::leave_message))
        .route("/getleavemsgs", get(book::get_leavetomsg_list))
        .route("/deleteleavemsg/:openid/:id", post(book::delete_leavemsg))
        .route("/getpubbooks", get(book::get_pub_book_list))
        .route("/getmybooks", get(book::get_my_book_list))
        .route("/addmybook", post(book::add_mybook))
        .route("/sharemybook/:openid/:id", post(book::share_mybook))
        .route("/deletemybook/:openid/:id", post(book::delete_mybook))
        .with_state(shared_state)
}
