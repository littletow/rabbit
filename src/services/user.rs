use anyhow::Ok;
use sqlx::SqlitePool;

use crate::stores::sql;
// 创建用户，srctype 来源类型 0 为公众号 1 为小程序
pub async fn create_user(
    pool: &SqlitePool,
    openid: String,
    nickname: String,
    srctype: i32,
) -> anyhow::Result<()> {
    sql::create_user(&pool, openid, nickname, srctype).await?;
    Ok(())
}
// 删除用户
pub async fn remove_user(pool: &SqlitePool, openid: String) -> anyhow::Result<()> {
    sql::remove_user(&pool, openid).await?;
    Ok(())
}
// 获取用户数量
pub async fn get_user_count(pool: &SqlitePool, openid: String) -> anyhow::Result<i64> {
    let cnt = sql::query_user_count(&pool, openid).await?;
    Ok(cnt)
}
