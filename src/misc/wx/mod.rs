mod wechat;
use crate::misc::utils;
pub use wechat::get_miniapp_qrcode;
pub use wechat::get_miniapp_qrcode_limit;
pub use wechat::get_mp_access_token;
pub use wechat::get_openid_bycode;
pub use wechat::upload_gzh_image;
pub use wechat::upload_gzh_image_v2;
pub use wechat::ImageReply;
pub use wechat::Message;
pub use wechat::Reply;
pub use wechat::TextReply;

// 请求默认AGENT
pub const DEFAULT_USER_AGENT: &'static str = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3534.4 Safari/537.36";

/// 微信接口域名
pub const API_DOMAIN: &'static str = "https://api.weixin.qq.com";

pub fn get_url_encode(c: &str) -> String {
    use percent_encoding::{utf8_percent_encode, AsciiSet, CONTROLS};
    const FRAGMENT: &AsciiSet = &CONTROLS
        .add(b' ')
        .add(b'"')
        .add(b'<')
        .add(b'>')
        .add(b'`')
        .add(b'+')
        .add(b'=')
        // .add(b'/')
        ;
    utf8_percent_encode(c, FRAGMENT).to_string()
}

pub fn check_mp_sign(sign: String, token: String, timestamp: String, nonce: String) -> bool {
    let mut params = Vec::new();
    params.push(token);
    params.push(timestamp);
    params.push(nonce);
    params.sort();
    let new_param_str = params.iter().map(|x| x.to_string()).collect::<String>();
    let sha1_data = utils::sha1(&new_param_str);
    sign == sha1_data
}
