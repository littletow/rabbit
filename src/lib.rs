// 库文件
pub mod errors;
pub mod handlers;
pub mod misc; // 工具类模块
pub mod services;
pub mod settings; // 定义设置模块
pub mod stores; // 服务模块

pub type Result<T> = std::result::Result<T, errors::AppError>;

use futures::channel::mpsc;
use redis::Client;
use serde::{Deserialize, Serialize};
use settings::Settings;
use sqlx::{Pool, Sqlite};
use std::{
    collections::{HashMap, HashSet},
    sync::{Arc, Mutex},
};
use tokio::sync::broadcast;

type SharedState = Arc<AppState>;

#[derive(Debug, Clone)]
pub struct AppState {
    pub db: Pool<Sqlite>,
    pub conf: Settings,
    pub rs: Client,
    pub ws: Arc<WsClient>,
}

type Tx = mpsc::UnboundedSender<WsMsg>;
type PeerMap = Mutex<HashMap<String, WsUserInfo>>;

#[derive(Debug)]
pub struct WsClient {
    pub peer_map: PeerMap,
    pub tx: broadcast::Sender<String>,
}
#[derive(Debug, Clone)]
pub struct WsUserInfo {
    pub openid: String,
    pub name: String,
    pub tx: Tx,
}

impl From<&WsUserInfo> for WsUserInfo {
    fn from(value: &WsUserInfo) -> Self {
        Self {
            openid: value.openid.clone(),
            name: value.name.clone(),
            tx: value.tx.clone(),
        }
    }
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WsMsg {
    pub from: String,
    pub dest: String,
    pub content: String,
    pub send_time: i64,
}

/// 用户Session
#[derive(Serialize, Deserialize, Debug, Default)]
pub struct UserSession {
    pub openid: String,
    pub name: String,
}
const SESSION_KEY: &str = "rabbit_session";
