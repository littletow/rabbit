// 配置文件
use config::{Config, ConfigError, Environment, File};
use serde::Deserialize;
use std::env;
#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct DatabaseConfig {
    pub dsn: String,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct RedisConfig {
    pub url: String,
}
#[derive(Debug, Deserialize, Clone)]
pub struct MpConfig {
    pub token: String,
    pub robot_url: String,
    pub aes_key: String,
    pub is_encrypt: bool,
    pub app_id: String,
    pub app_secret: String,
    pub gzh_app_id: String,
    pub gzh_app_secret: String,
    pub shared_key: String,
}
#[derive(Debug, Deserialize, Clone)]
pub struct EmailConfig {
    pub host: String,
    pub port: u32,
    pub user_name: String,
    pub password: String,
    pub is_ssl: bool,
}
#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct Settings {
    pub debug: bool,
    pub database: DatabaseConfig,
    pub redis: RedisConfig,
    pub mp: MpConfig,
    pub email: EmailConfig,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let run_mode = env::var("RUN_MODE").unwrap_or_else(|_| "dev".into());
        let s = Config::builder()
            .add_source(File::with_name(&format!("config/{}", run_mode)).required(false))
            .add_source(Environment::with_prefix("app"))
            .build()?;
        s.try_deserialize()
    }
}
