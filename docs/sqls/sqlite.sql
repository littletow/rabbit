# 创建用户表
CREATE TABLE IF NOT EXISTS mp_users
(
    id          INTEGER PRIMARY KEY NOT NULL,
    openid varchar(60)                NOT NULL,
    nickname varchar(200),
    createtime bigint,
    updatetime bigint
);
# 添加用户来源类型，0 公众号 1 小程序
ALTER TABLE mp_users ADD COLUMN srctype int DEFAULT 0;

# 创建用户统计表，follow状态 1 关注 2 取消关注
CREATE TABLE IF NOT EXISTS mp_user_statistics
(
    id          INTEGER PRIMARY KEY NOT NULL,
    statdate varchar(20) NOT NULL,
    followstatus int NOT NULL,
    logts timestamp DEFAULT (datetime('now','localtime'))
);

# 创建用户统计触发器，用户关注
CREATE TRIGGER "userfollow"
AFTER INSERT
ON "mp_users"
BEGIN
   INSERT INTO mp_user_statistics(statdate, followstatus) VALUES (date('now'),1);
END;


# 调整触发器
DROP TRIGGER "userfollow";

CREATE TRIGGER "userfollow"
AFTER INSERT
ON "mp_users"
WHEN new.srctype==0
BEGIN 
   INSERT INTO mp_user_statistics(statdate, followstatus) VALUES (date('now'),1);
END;

# 创建用户统计触发器，用户取消关注
CREATE TRIGGER "userunfollow"
AFTER DELETE
ON "mp_users"
BEGIN
   INSERT INTO mp_user_statistics(statdate, followstatus) VALUES (date('now'),2);
END;

# 调整触发器
DROP TRIGGER "userunfollow";

CREATE TRIGGER "userunfollow"
AFTER DELETE
ON "mp_users"
WHEN old.srctype==0
BEGIN
   INSERT INTO mp_user_statistics(statdate, followstatus) VALUES (date('now'),2);
END;

# 查询触发器
SELECT name FROM sqlite_master
WHERE type = 'trigger';

# 创建图书表
create table if not exists mp_books
(
    id          INTEGER PRIMARY KEY NOT NULL,
    openid varchar(60)                NOT NULL,
    name varchar(200),
    press varchar(200),
    isbn varchar(200),
    isshare int,
    createtime bigint,
    updatetime bigint
);

# 扩展审核字段 status 1 正常 2 审核失败 0 待审核
ALTER TABLE mp_books ADD COLUMN status int;
# 图书添加唯一索引
CREATE UNIQUE INDEX idx_isbn on mp_books (isbn);
# 用户昵称添加唯一索引
CREATE UNIQUE INDEX idx_nickname on mp_users (nickname);

# 第三方账号申请表
CREATE TABLE IF NOT EXISTS app_accounts
(
    id          INTEGER PRIMARY KEY NOT NULL,
    openid varchar(60)                NOT NULL,
    appid varchar(100),
    appsecret varchar(200),
    status int, 
    createtime bigint,
    updatetime bigint
);

CREATE UNIQUE INDEX idx_appid on app_accounts (appid);
CREATE UNIQUE INDEX idx_openid on app_accounts (openid);

# 留言表
create table if not exists mp_msgs
(
    id          INTEGER PRIMARY KEY NOT NULL,
    fromopenid varchar(60)                NOT NULL,
    toopenid varchar(60) NOT NULL,
    msg varchar(2000),
    createtime bigint
);

# 警言妙句
create table if not exists mp_sentences
(
    id          INTEGER PRIMARY KEY NOT NULL,
    msg varchar(2000),
    createtime bigint
)